#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Author: Quantil - Data Mining Group
"""

import sys
sys.path.append("../..")

import numpy as np 
import pandas as pd

from scipy.stats import norm

import torch
import functools
import operator
import copy

from tqdm import tqdm

EMBEDDING_PATH = 'GloVe/ckpt/lemma/saved_models/epoch_69_ckpt.pth'
DIC_PATH = 'data_default/lemma/token2id.pth'
class LabelManager():
    def __init__(self):
        """
        Description:
             Init Label Manager
        """
        
        '''
        Get GloVe train model
        '''
        device = torch.device("cpu")
        model = torch.load(EMBEDDING_PATH, map_location=device)
        focal_embeds = model['model_state_dict']['_focal_embeddings.weight']
        context_embeds = model['model_state_dict']['_context_embeddings.weight']
        
        self._glove_vecs = 0.5*(focal_embeds + context_embeds)     # GloVe Model
        
        aux = torch.load(DIC_PATH)
        self._token2id = aux.get('token2id')     # Token and id dictionary
        
        '''
        Lables identifier
        Lables (First, Dictionary, Filter Dictionary)
        '''
        self.id_labels = None
        self.labels_docs = None
        
        self.new_labels_docs = None
        self.final_labels = None

    def set_labels_docs(self, docs_labels, id_labels=[]):
        """
        Description:
            Set initial doc lables
        Args:
            docs_lables (list of str): Initial documents of lables
            id_lables (list of str(float)): Label identifiers
        """
        self.id_labels = id_labels
        self.labels_docs = docs_labels

    def reset(self):
        """
        Description:
            Reset lables
        """
        self.labels_docs = None
        self.new_labels_docs = None
        self.final_labels = None

    def __tf(self, word, document):
        """
        Description:
            Compute term-frecuency word in document
        Args:
            word (str): Requested word
            documnets (str): Requested document
        Returns:
            float: Term-frecuency number
        """
        
        words_doc = np.array(document.split(), dtype=str)    # Split documents
        arr_word = np.array([word], dtype=str)    # Word as vector

        '''
        Term-Frecuency
        '''
        count_word = sum(np.isin(words_doc, arr_word))
        
        res = count_word / len(words_doc)

        return res


    def __fac(self, w, c):
        """
        Description:
            Compute Function-component aware
        
        Args:
            w (str): Requested word
            c (str): Requested dictionary
        
        Returns:
            float: FAC number
        """
        
        M = len(self.new_labels_docs)

        tf_wc = self.__tf(w, c)    # Term-frecuency 
        
        '''
        Term-frecuency of each word
        '''
        all_tf = [self.__tf(w, k) for k in self.new_labels_docs if k != c] + [tf_wc]
        
        '''
        FAC
        '''
        average = (1 / M) * (sum(all_tf))

        var_tf = np.var(np.array(all_tf[:-1]))
        
        fac = (tf_wc - average) / (var_tf + 1e-9)    # Term 1e-9 avoid zero divition
        
        return fac

    def __get_similarity(self, word, kNN):
        """
        Description:
            Get first kNN similar words acording GloVe
            
        Args:
            word (str): Requested word
            kNN (int): k-nearest words
            
        Returns:
            list of str: list of k-nearest words
        """
        W = self._glove_vecs    # GloVe model
        token2id = self._token2id
        x = W[token2id[word]]     # word embedding
            
        '''
        Cosine similarity
        '''
        cos = torch.matmul(W, x) / (torch.sum(W * W, dim=-1) * torch.sum(x * x) + 1e-9).sqrt()
        _, topk = torch.topk(cos, kNN+1)
        
        # id2token = {idx: token for idx, token in enumerate(self._token2id.keys())}

        nearest_neighbors = topk[1:].tolist()
        nearest_neighbors = [list(self._token2id.keys())[comp] for comp in nearest_neighbors]    # Nearest words
    
        return nearest_neighbors
    
 
    def __create_labels(self, kNN):
        """
        Description:
            Set full dictionary for each label (Enrichment)
        
        Args:
            kNN (int): k-nearest words for each label's word 
        """
        if not self.labels_docs:
            raise Exception('No hay labels')
        
        pos_labels = []
        
        '''
        Enrichment step
        '''
        for doc in tqdm(self.labels_docs, desc='Create new labels'):
            # tokens = doc.split()
            tokens = set(doc.split()).intersection(self._token2id.keys())
            label = [doc]
            for token in tokens:
                # if token in self._token2id.keys():
                to_add = self.__get_similarity(token, kNN)
                label += to_add

            pos_labels.append(' '.join(label))

        self.new_labels_docs = pos_labels
            

    def __avoid_overlapping(self, fac_cote):
        """
        Description:
            Use FAC threshold for avoid overlapping (Consolidation)
        
        Args:
            fac_cote (float): FAC threshold
        """
        labels = self.new_labels_docs if self.new_labels_docs else self.labels_docs
        
        f_labes = []
        pos_labels = []
        min_len = np.inf
        '''
        Consolidation step
        '''
        for label in tqdm(labels, desc='Avoiding Overlapping'):
            words = set(label.split())
            new_label = []
            fac_labes = []
            for w in words:
                fac_w = self.__fac(w, label)

                if fac_w >= fac_cote:
                    new_label.append(w)
                    fac_labes.append(fac_w)
            
            pos_labels.append((new_label, fac_labes))
            min_len = len(new_label) if len(new_label) < min_len else min_len
            f_labes.append(' '.join(new_label))

        self.final_labels = f_labes

    def process_labels(self, kNN=3, fac_cote=20, overlapping_step=False):
        """
        Description:
            Prepare new lables.
        Args:
            kNN (int): k-nearest words for each label's word 
            fac_cote (float): FAC threshold
        """
        self.__create_labels(kNN)
        if overlapping_step:
            self.__avoid_overlapping(fac_cote)
            

        
        

        