#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Author: Jose Sebastián Ñungo, Researcher, Quantil
        (email: sebastian.nungo@quantil.com.co)
"""

import sys
sys.path.append("../..")

import numpy as np
import pandas as pd
from tqdm import tqdm

from processingText import ProcessingText

from treelib import Node, Tree

import googletrans

import torch
import functools
import operator

class SkillsManager():

    def __init__(self,
                    path_hierarchy_skills='model/Tree/ESCO/skillsHierarchy_es.csv',
                    path_lang_skills='model/Tree/ESCO/languageSkillsCollection_es.csv',
                    path_raw_skills='',
                    lemma=True):
        """
        Description:
            Init Occupation Manager
        
        Args:
            path_hierarchy_skills (str): Hierarchy skills file path
            path_lang_skills (str): Langue skills file path
            lemma (bool): Lemmatize skills texts condition
        """

        _skills_hier = pd.read_csv(path_hierarchy_skills)
        
        self._skills_hier = self.__translate_DB(_skills_hier)

        self._lang_collection = pd.read_csv(path_lang_skills)
        self._processer = ProcessingText()
        
        self.main_tree, self.id_nodes = self.__create_tree(lemma=lemma)

    def __translate_DB(self, skills_db):
        """
        Description:
            Translate skills data base
        
        Args:
            skills_db (DataFrame): skills DB
        
        Returns:
            DataFrame: Translated skills data base
        """
        skills_db = skills_db.fillna('xyx')    
        translator = googletrans.Translator()     # Google translate

        trans = []
        for desc in tqdm(skills_db.Description.values, desc='EN-ES DB'):
            if desc == 'xyx':
                trans.append('')
            else:
                trd = translator.translate(desc.split('\n')[0], src='en', dest='es').text
                trans.append(trd)     # Translated text

        skills_db['Description'] = trans

        return skills_db


    def __create_tree(self, lemma):
        """
        Description:
            Create skills tree
            
        Args:
            lemma (bool): Lemmatize skills texts condition
            
        Returns:
            Tree: skills tree
            list of str: identifieres of skills
        """
        root_skills_tree = Tree()
        root_skills_tree.create_node('Habilidades', 'H')
        ids_n = []                

        identifiers = {}

        for i in tqdm(range(4), desc='Main Tree'):
            name_col = f'Level {i} preferred term'
            
            node_names = list(self._skills_hier[name_col].unique().astype(str))

            if 'xyx' in node_names:
                node_names.remove('xyx')

            if i < 3:
                name_sig_col = f'Level {i+1} preferred term'
                cond = np.logical_and(self._skills_hier[name_col].isin(node_names).values, self._skills_hier[name_sig_col].values =='xyx' )
                tree_df = self._skills_hier.loc[cond].copy()
            else:
                cond = self._skills_hier[name_col].values != 'xyx'
                tree_df = self._skills_hier.loc[cond].copy()
            

            for j, n_name in enumerate(node_names):
                if i == 0:
                    n_id = str(j + 1)
                    ids_n.append(n_id)
                    n_name = n_name
                    data_int = list(tree_df.loc[tree_df[name_col] ==  n_name, [name_col, 'Description']].values[0].astype(str))
                    data_pp = self._processer.processing_corpus(data_int, lemma=lemma)
                    
                    aux_name =  n_name + str(i)

                    n_data = {
                        'preferred_label': ' '.join(data_pp[0]),
                        'description': ' '.join(data_pp[1]),
                    }
                    root_skills_tree.create_node(tag=n_name, identifier=n_id, data=n_data, parent='H')
                    
                    identifiers[aux_name] = [n_id, 0]
                else:
                    name_parent = str(tree_df.loc[tree_df[name_col] == n_name, f'Level {i-1} preferred term'].values[0])

                    name_parent += str(i - 1)

                    id_parent, children_num = identifiers.get(name_parent)

                    identifiers[name_parent] = [id_parent, children_num + 1]

                    n_id = str(id_parent) + '-' +str(children_num + 1)
                    ids_n.append(n_id)
                    n_name = n_name
                    
                    data_int = list(tree_df.loc[tree_df[name_col] ==  n_name, [name_col, 'Description']].values[0].astype(str))
                    data_pp = self._processer.processing_corpus(data_int, lemma=lemma)

                    n_data = {
                        'preferred_label': ' '.join(data_pp[0]),
                        'description': ' '.join(data_pp[1]),
                    }

                    root_skills_tree.create_node(tag=n_name, identifier=n_id, data=n_data, parent=id_parent)

                    aux_name =  n_name + str(i)
                    identifiers[aux_name] = [n_id, 0]
        
        
        id_root = '3-1' if root_skills_tree.get_node('3-1').tag == 'Lenguas' else '3-2'

        langues_desc = self._lang_collection.loc[(self._lang_collection.broaderConceptPT == 'lenguaje | Lenguas')
                                                | (self._lang_collection.broaderConceptPT == 'Lenguas | lenguaje'),
                                                ['preferredLabel', 'description']
                                            ].values
        
        for l, lang_desc in enumerate(tqdm(langues_desc, desc='Add Languagues')):
            id_parent = id_root

            n_id = id_parent + f'-{l}'
            ids_n.append(n_id)
            n_name = lang_desc[0]

            data_pp = self._processer.processing_corpus(lang_desc, lemma=lemma)

            n_data ={
                'preferred_label': ' '.join(data_pp[0]),
                'description': ' '.join(data_pp[1])
            }
            root_skills_tree.create_node(tag=n_name, identifier=n_id, data=n_data, parent=id_parent)

            children_lang = self._lang_collection.loc[self._lang_collection.broaderConceptPT == n_name,
                                                        ['preferredLabel', 'description']
                                                ].values

            id_parent = n_id
            for c, child in enumerate(children_lang):
                
                n_id = id_parent + f'-{c}'
                ids_n.append(n_id)    
                n_name = child[0]

                data_pp = self._processer.processing_corpus(lang_desc, lemma=lemma)

                n_data ={
                    'preferred_label': ' '.join(data_pp[0]),
                    'description': ' '.join(data_pp[1])
                }
                
                root_skills_tree.create_node(tag=n_name, identifier=n_id, data=n_data, parent=id_parent)


        return root_skills_tree, ids_n
    
    def skill_subtree_document(self, id_skill):
        """
        Description:
            Create skills document
        Args:
            id_skill (str): Skills identifier
        Returns:
            str: Documnet
        """
        # Generate sub tree given id_ocupation
        sub_tree = self.main_tree.subtree(id_skill)
        # Doocument generate by
        doc_1 = ' '.join([sub_tree[node].data.get('description', ' ') for node in sub_tree.expand_tree(mode=Tree.DEPTH)])
        aux = functools.reduce(
                                operator.iconcat,
                                [sub_tree[node].data.get('preferred_label').split() for node in sub_tree.expand_tree(mode=Tree.DEPTH)]
                        )

        aux = ' '.join(aux)
        
        doc = doc_1 + aux
        return doc
    
    def skill_desc_subtree(self, id_skill):
        """
        Description:
            Create skills document (only descriptions) 
        Args:
            id_skill (str): Skills identifier
        Returns:
            str: Documnet
        """
        sub_tree = self.main_tree.subtree(id_skill)

        doc = []
        for node in sub_tree.expand_tree(mode=Tree.DEPTH):
            dic_node = sub_tree[node].data
            if 'description' in dic_node.keys():
                doc.append(dic_node.get('description'))

        return doc
    
    def skill_desc(self, id_skill):
        """
        Description:
            Get occupation description
        
        Args:
            id_skill (str): Occupation identifier
        Returns:
            str: Description
        """
        node_dic = self.main_tree.get_node(id_skill).data
        res = ''
        if 'description' in node_dic.keys():
            res = node_dic.get('description')
        if res == '':
            res = node_dic.get('preferred_label')
            
        return res

    ## given ocupation id return children identifier nodes in oreder to reexplore particular tags
    def children_nodes(self, id_skill):
        """
        Description:
            Get children nodes
        
        Args:
            id_skill (str): Skill identifier
        Returns:
            list of str: id of nodes
        """
        children = self.main_tree.children(id_skill)

        children_ids = list(map(lambda x: x.identifier, children))

        return children_ids

    def depth_index(self, depth):
        """
        Description:
            Get id of nodes at depth
        Args:
            depth (int): Tree depth
        Returns:
            list of str: id of nodes
        """
        res = None
        if depth > 0:
            res = [idx for idx in self.id_nodes if len(idx.split('-')) == depth]

        return res

    def subtree_index(self, root_idx):
        """
        Description:
            Get id of nodes with same root
        Args:
            root_idx (str): Root identifier
        Returns:
            list of str: id of nodes        
        """
        res = None
        if root_idx in self.id_nodes:
            res = [idx for idx in self.id_nodes if (len(idx) > len(root_idx) and idx[:len(root_idx)] == root_idx) ]
        return res
