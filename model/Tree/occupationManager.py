#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Author: Jose Sebastián Ñungo, Researcher, Quantil
        (email: sebastian.nungo@quantil.com.co)
"""

import sys
sys.path.append("../..")

import re
import functools
import operator

from processingText import ProcessingText

import pandas as pd
import numpy as np
from treelib import Node, Tree
from tqdm import tqdm

# import googletrans
# import goslate


class OccupationManager():

    ## path of ISCO file and ocupations file
    def __init__(self,
                    path_ISCO='model/Tree/ESCO/ISCOGroups_es.csv', 
                    path_OCUP='model/Tree/ESCO/occupations_es.csv',
                    path_ISCO_en='model/Tree/ESCO/en/ISCOGroups_en.csv', 
                    create_main_tree=True,
                    lemma=True,
                    translate=False,
                    only_ISCO=False):

        """
        Description:
            Init Occupation Manager
        
        Args:
            path_ISCO (str): ISCO file path
            path_OCUP (str): Occupation file path
            path_ISCO_en (str): ISCO english file path
            create_main_tree (bool): Create occupation main tree condition
            lemma (bool): Lemmatize occupations texts condition
            translate (bool): Translate ISCO text condition
            only_ISCO (bool): Create only ISCO tree condition
        """
        ## Ocupaciones
        self._isco_g_df = pd.read_csv(path_ISCO)
        self._isco_g_df_en = pd.read_csv(path_ISCO_en)
        self._ocupaciones_df = pd.read_csv(path_OCUP)
        self._processer = ProcessingText()
        self.id_nodes = None
        self.main_tree = None

        if create_main_tree:
            self.main_tree, self.id_nodes = self.__create_tree(lemma, translate, only_ISCO)


    def __create_tree(self,lemma, translate, only_ISCO):
        """
        Description:
            Create occupation tree
            
        Args:
            lemma (bool): Lemmatize occupations texts condition
            translate (bool): Translate ISCO text condition
            only_ISCO (bool): Create only ISCO tree condition
        
        Returns:
            Tree: occupation tree
            list of str: identifieres of occupations
        """
        ocupations_tree = Tree()
        ids_n = []
        ocupations_tree.create_node('Ocupaciones', 'O')
        ## Remove militar ocupations
        isco_no_0 = self._isco_g_df[10:].fillna('')

        ## Take aditional list of corpuos (of labels), but preprocessed. For data in each node
        labels_pp = self._processer.processing_corpus(isco_no_0.preferredLabel.values, lemma=lemma)

        ##
        descr_occ = self._processer.processing_corpus(isco_no_0.description.values, lemma=lemma)

        '''
            CONSTRUCT MAIN PART OF THE TREE
        '''

        for i in tqdm(range(len(isco_no_0)), desc='Main Tree'):
            n_tag = isco_no_0.iloc[i].preferredLabel
            n_id = '-'.join(list(str(int(isco_no_0.iloc[i].code))))

            if translate:
                n_data = {
                    'ppLabels': ' '.join(labels_pp[i]),
                    'description': ' '.join(descr_occ[i])
                }
            else:
                n_data = {  'ppLabels': ' '.join(labels_pp[i])  }

            if len(n_id) == 1:
                ocupations_tree.create_node(tag=n_tag, identifier=n_id, parent='O', data=n_data)
            else:
                n_parent = n_id[:-2]
                ocupations_tree.create_node(tag=n_tag, identifier=n_id, parent=n_parent, data=n_data)

            ids_n.append(n_id)


        if not only_ISCO:
            '''
                Add last levels to tree
            '''

            ## Remove militar ocupations
            key_0_ocup = self._ocupaciones_df.iscoGroup.apply(lambda x: True if len(str(x)) == 3 else False)
            ocupations_no_0 = self._ocupaciones_df.loc[~key_0_ocup].copy()

            ## Join all possibles lables of each last levles ocupations
            ocupations_no_0['AllLabels'] = ocupations_no_0[
                                                            ['preferredLabel', 'altLabels', 'hiddenLabels']
                                                        ].fillna(' ').apply(lambda x: ' '.join(x), axis=1)

            labels_pp_last = self._processer.processing_corpus(ocupations_no_0.AllLabels.values, lemma=lemma)
            ## Add processed labels
            ocupations_no_0['ppLabels'] = labels_pp_last

            ## Keys of latests levels
            key_ref_last_level = isco_no_0.code.apply(lambda x: True if len(str(x)) == 4 else False)
            ## Latests levles
            ref_last_level = isco_no_0.loc[key_ref_last_level].copy()

            for i in tqdm(range(len(ref_last_level)), desc='Leaves'):
                n_parent = '-'.join(list(str(int(ref_last_level.iloc[i].code))))
                ## Childrens of provitional tree last level
                n_children = ocupations_no_0.loc[ocupations_no_0.iscoGroup == ref_last_level.iloc[i].code, 
                                                    [
                                                        'preferredLabel',
                                                        'altLabels',
                                                        'hiddenLabels',
                                                        'description',
                                                        'AllLabels',
                                                        'ppLabels'
                                                    ]
                                                ].fillna('').values

                if len(n_children) > 0:
                    for j, n_child in enumerate(n_children):
                        n_tag = n_child[0]
                        n_id = n_parent + '-' + str(j + 1)
                        pp_n_child = self._processer.processing_corpus(n_child[:5], lemma=lemma)

                        n_data = {  
                                    'preferredLabel': ' '.join(pp_n_child[0]),
                                    'altLabels': ' '.join(pp_n_child[1]),
                                    'hiddenLabels': ' '.join(pp_n_child[2]),
                                    'description': ' '.join(pp_n_child[3]),
                                    'AllLables': ' '.join(pp_n_child[4]),
                                    'ppLabels': ' '.join(n_child[5])
                                }
                        ocupations_tree.create_node(tag=n_tag, identifier=n_id, parent=n_parent, data=n_data)
                        ids_n.append(n_id)
        return ocupations_tree, ids_n


        ## given ocupation id return the document associate
    def ocupation_subtree_document(self, id_ocupation):
        """
        Description:
            Create occupation document
        Args:
            id_ocupation (str): Occupation identifier
        Returns:
            str: Documnet
        """
        # Generate sub tree given id_ocupation
        sub_tree = self.main_tree.subtree(id_ocupation)
        # Doocument generate by
        doc_1 = ' '.join([sub_tree[node].data.get('description', ' ') for node in sub_tree.expand_tree(mode = Tree.DEPTH)])
        aux = functools.reduce(
                                operator.iconcat,
                                [sub_tree[node].data.get('ppLabels', ' ').split() for node in sub_tree.expand_tree(mode=Tree.DEPTH)]
                            )

        aux = ' '.join(aux)
        doc = doc_1 + aux
        return doc

    def occ_desc_subtree(self, id_occupacion):
        """
        Description:
            Create occupation document (only descriptions) 
         Args:
            id_ocupation (str): Occupation identifier
        Returns:
            str: Documnet
        """
        sub_tree = self.main_tree.subtree(id_occupacion)

        doc = []
        for node in sub_tree.expand_tree(mode=Tree.DEPTH):
            dic_node = sub_tree[node].data
            if 'description' in dic_node.keys():
                doc.append(dic_node.get('description'))

        return doc

    def occ_desc(self, id_occupacion):
        """
        Description:
            Get occupation description

        Args:
            id_ocupation (str): Occupation identifier
        Returns:
            str: Description
        """
        node_dic = self.main_tree.get_node(id_occupacion).data
        res = ''
        if 'description' in node_dic.keys():
            res = node_dic.get('description')

        return res


        ## given ocupation id return children identifier nodes in oreder to reexplore particular tags
    def children_nodes(self, id_ocpuation):
        """
        Description:
            Get children nodes

        Args:
            id_ocupation (str): Occupation identifier
        Returns:
            list of str: id of nodes
        """
        children = self.main_tree.children(id_ocpuation)

        children_ids = list(map(lambda x: x.identifier, children))

        return children_ids

    def depth_index(self, depth):
        """
        Description:
            Get id of nodes at depth
        Args:
            depth (int): Tree depth
        Returns:
            list of str: id of nodes
        """
        res = None
        if depth > 0:
            res = [idx for idx in self.id_nodes if len(idx.split('-')) == depth]

        return res

    def subtree_index(self, root_idx):
        """
        Description:
            Get id of nodes with same root
        Args:
            root_idx (str): Root identifier
        Returns:
            list of str: id of nodes        
        """
        res = None
        if root_idx in self.id_nodes:
            res = [idx for idx in self.id_nodes if (len(idx) > len(root_idx) and idx[:len(root_idx)] == root_idx) ]

        return res

    '''
    Inyective match
    '''
    def __possible_label(self, node, process=True):
        labels = []
        # preferred_l = re.sub('/a ', ' ', node.data['preferredLabel'])
        alt_l = node.data['altLabels']
        hidden_l = node.data['hiddenLabels']

        # labels.extend(preferred_l.split('/'))
        labels.extend(alt_l.split('\n'))
        labels.extend(hidden_l.split('\n'))
        if process:
            aux = self._processer.processing_corpus(labels)
            labels = list(np.setdiff1d(np.unique(np.array([' '.join(l) for l in aux])), ['']))

        return labels

    ## with given corpus the function find in which documents (by index) are the occupations
    # ## in a literal term matching
    # def one2one_match(self, corpus, depth, lemma=True):
    #     id_nodes_int = self.depth_index(depth)
    #     match = []

    #     for index in id_nodes_int:
            
    #     if depth == 5:
    #         for identifier in tqdm(id_nodes_int, desc='Leaves CtrF'):
    #             node_i = self.main_tree.get_node(identifier)
    #             if lemma:
    #                 possibles_labels = self.__possible_label(node_i)
    #             else:
    #                 possibles_labels = self.__possible_label(node_i, process=False)

    #             index_match = []
    #             for t, text in enumerate(corpus):
    #                 for label in possibles_labels:
    #                     if label in text:
    #                         index_match.append(t)
    #                         break

    #             if index_match:
    #                 tuple_match = (identifier, index_match)
    #                 match.append(tuple_match)

    #     else:
    #         for identifier in tqdm(id_nodes_int, desc='Tree CtrF'):
    #             if lemma:
    #                 label = self.main_tree.get_node(identifier).data['ppLabels']
    #             else:
    #                 label = self.main_tree.get_node(identifier).tag

    #             index_match = []
    #             for t, text in enumerate(corpus):
    #                 if label in text:
    #                     index_match.append(t)

    #             if index_match:
    #                 tuple_match = (identifier, index_match)
    #                 match.append(tuple_match)

    #     return match