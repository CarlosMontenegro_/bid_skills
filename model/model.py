#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Author: Quantil - Data Mining Group
"""
import sys
import warnings

sys.path.append("..")

import numpy as np
import pandas as pd
import torch

from tqdm import tqdm

# Data preprocessor class
from processingText import ProcessingText

# ESCO TREES
from .Tree.occupationManager import OccupationManager
from .Tree.skillsManager import SkillsManager
from .Tree.labelManager import LabelManager

from gensim import corpora, models

PATH_MODEL = './model/model/lsi_models.pth'
PATH_ESCO = './model/model/esco.pth'



class ClassificationModel():

    def __init__(self,
                path_data='data_default/sample_db.csv',
                ):
        """
        Description:
             Init Classification Model.
         Args:
             path_data (str): Path of documents
             
             CSV columns format: descripcion, titulo, country
             
        """
        
        self._processer = ProcessingText()   # 

        data = pd.read_csv(path_data)    # Read documents data
        self._data = data.fillna(' ')    # Fix nan values
        self.sample_df = self._data      # Default sample data
        
        
        '''
            Pre-made models handeling
        '''
        self._l_manager_s = None
        self._l_manager_o = None
        
        self._s_manager = None
        self._o_manager = None
        
        # Handeling models
        self.__load_lables_managers()
    
        
        # Pre-made LSA
        self._dct_occ = self._dct_skills = None
        self._lsi_occ = self._lsi_skills = None
        self.__load_model()
    
        
    def __load_model(self):
        '''
        Description:
            Load pre-made LSA model, and dictionaries
        '''
        device = torch.device("cpu")
        to_dic = torch.load(PATH_MODEL, map_location = device)
        
        self._dct_occ, self._lsi_occ = to_dic['occ_model']
        self._dct_skills, self._lsi_skills = to_dic['skills_model']

        args = {'num_topics': -1, 'num_words': 1}

        self._num_topics_occ = len(self._lsi_occ.print_topics(**args))
        self._num_topics_skills = len(self._lsi_skills.print_topics(**args))


    def __load_lables_managers(self):
        '''
        Description:
            Load pre-made dictionary
        '''
        device = torch.device("cpu")
        to_dic = torch.load(PATH_ESCO, map_location=device)
        
        self._l_manager_s = to_dic['l_manager_s']
        self._l_manager_o = to_dic['l_manager_o']
        self._s_manager = to_dic['s_manager']
        self._o_manager = to_dic['o_manager']
                


    def __process_sample_data(self):
        """
        Description:
            Process sample data
        """
        files = self.sample_df['descripcion']
        files = list(files.to_numpy())     # Descriptions

        doc_text = self._processer.processing_corpus(files, tqdm_track=True)

        files = self.sample_df['titulo']
        files = list(files.to_numpy())      # Titles
        
        title_text = self._processer.processing_corpus(files, tqdm_track=True)
        
        doc_country = self.sample_df['country'].values
        
        doc_ids = self.sample_df['id'].values
        
        self.doc_text_filter = [
                                (index, tit, doc, country) 
                                 for index, tit, doc, country in list(zip(doc_ids, title_text, doc_text, doc_country))
                                 if (len(doc) > 0 and len(tit) > 0)
                                ]
        
    
    def set_sample_data(self, demo_sample=1):
        """
        Description:
        Process and set sample data.
        
        Args:
            demo_sample (float): Fraction of items to return
        """
        if demo_sample < 1:
            # sample from our dataset in the same proportion as countries
            if 'country' in self._data.columns:
                self.sample_df = self._data.groupby(
                                                'country',
                                                as_index=False
                                            ).apply(
                                                lambda x: x.sample(frac=demo_sample)
                                            )
            else:
                self.sample_df = self._data.sample(frac=demo_sample).reset_index(drop=True)
        else:
            self.sample_df = self._data
        
        self.__process_sample_data()
    
    def retrain_lsi_model(self,
                            corpus=None,
                            processed_data=None,
                            num_topics=100,
                            skills_model=False,
                            occupation_model=False):
        '''
        Description:
            Set a retrained LSA model and new dictionary with new documents.
        
        Args:
            processed_data (list of list of str): Processed documnets (See ProcessingText).
            num_topics (int): Number of requested factors (latent dimensions)
            skills_model (bool): Conditional to update skills lsi model
            occupation_model (bool): Conditional to update occupations lsi model
        '''
        if not (skills_model or occupation_model):
            print('Both skills_model and occupation_model parameters are False')
        else:
            if corpus:
                processed_data = self._processer.processing_corpus(corpus, tqdm_track=True)
            elif not (corpus and processed_data):
                processed_data = [pp_doc[1] for pp_doc in self.doc_text_filter]
                
            dct = corpora.Dictionary(processed_data)
            # dct.filter_extremes(no_above=.95)
            bow_corpus = [dct.doc2bow(doc) for doc in tqdm(processed_data)]
            
            args = {'num_topics': -1, 'num_words': 1}
            
            if skills_model:
                self._dct_skills = dct
                self._lsi_skills = models.LsiModel(
                                                bow_corpus,
                                                id2word=dct,
                                                num_topics=num_topics,
                                                power_iters=100
                                                )
                self._num_topics_skills = len(self._lsi_skills.print_topics(**args))
                print('New Skills LSI Model')
            if occupation_model:
                self._dct_occ = dct
                self._lsi_occ = models.LsiModel(
                                                bow_corpus,
                                                id2word=dct,
                                                num_topics=num_topics,
                                                power_iters=100
                                                )
                self._num_topics_occ = len(self._lsi_occ.print_topics(**args))
                print('New Occupations LSI Model')

    def set_new_lsi_models(self):
        """
            Save new LSI models
        """
        to_save = {
            'occ_model': [self._dct_occ, self._lsi_occ],
            'skills_model': [self._dct_skills, self._lsi_skills]
        }

        torch.save(to_save, PATH_MODEL)

    def reset_skills_manager(self, depth=3, specific_s=['1', '2'], 
                            fac_cote=8, kNN=2, overlapping_step=False):
        """
        Descripcion:
            Set new skills label manager.
            
        Args:
            depth (int): Depth of skills taxonomy.
            specific_s (list of str(int)): Root of requested skills identifiers.
            fac_cote (float): Function-component aware treshhold.
            kNN (int): GloVe k-nearest word.
        """
        self._l_manager_s = LabelManager()   # New Label Manager
               
        id_labels_s = self._s_manager.depth_index(depth)   # Identifiers of lables
        
        id_labels_s = [l for l in id_labels_s if l[0] in specific_s]   # Filter by root identifiers
        
        pp_labels_s = [self._s_manager.skill_subtree_document(i) for i in id_labels_s]   # Create lable dictionary

        self._l_manager_s.set_labels_docs(pp_labels_s, id_labels_s)    # Place initial Lables

        self._l_manager_s.process_labels(
                                    kNN = kNN,
                                    fac_cote = fac_cote,
                                    overlapping_step = overlapping_step
                                )    # Final labels
    
    def reset_occupations_manager(self, depth=2, fac_cote=13, kNN=3, overlapping_step=False):
        """
        Descripcion:
            Set new occupation label manager.
            
        Args:
            depth (int): Depth of occupation taxonomy.
            specific_s (list of str(int)): Root of requested skills identifiers.
            fac_cote (float): Function-component aware treshhold.
            kNN (int): GloVe k-nearest word.
        """
        self._l_manager_o = LabelManager()    # New label manager

        id_labels_o = self._o_manager.depth_index(depth)    # Identifiers of lables

        pp_labels_o = [self._o_manager.ocupation_subtree_document(i) for i in id_labels_o]    # Create lable dictionary

        self._l_manager_o.set_labels_docs(pp_labels_o, id_labels_o)    # Places initial Lables

        self._l_manager_o.process_labels(
                                    kNN = kNN,
                                    fac_cote = fac_cote,
                                    overlapping_step = overlapping_step
                                )    # Final labels

    def set_new_labels_mangers(self):
        """
            Save new models.
        """
        to_save = {
                'l_manager_o': self._l_manager_o,
                'o_manager': self._o_manager,
                'l_manager_s': self._l_manager_s,
                's_manager': self._s_manager
            }
        torch.save(to_save, PATH_ESCO)

    def __text_embedding(self, tit, doc, occ_cast):
        if occ_cast:
            dct, lsi_model = self._dct_occ, self._lsi_occ
            num_topics = self._num_topics_occ
        else:
            dct, lsi_model = self._dct_skills, self._lsi_skills
            num_topics = self._num_topics_skills
        
        text_bow = dct.doc2bow(doc)    # Description BoW
        text_vec = lsi_model[text_bow]    # Description vector
        
        tit_bow = dct.doc2bow(tit)    # Title BoW
        tit_vec = lsi_model[tit_bow]    # Title vector
        
        if not (text_vec and tit_vec):
            raise Exception('Null embedding')

        for v, vec in enumerate((tit_vec, text_vec)):
            if len(vec) < num_topics:
                aux = np.array(vec)
                vec = np.zeros(num_topics)
                vec[aux[:, 0].astype(int)] = aux[:, 1]
                # print(len(vec))
                if v == 0:
                    tit_vec = vec
                else:
                    text_vec = vec
            else:
                if v == 0:
                    tit_vec = np.array(tit_vec)[:, 1]
                else:
                    text_vec = np.array(text_vec)[:, 1]

        text_vec = torch.Tensor(text_vec)    # Torch vector
        tit_vec = torch.Tensor(tit_vec)  # Torch vector

        return text_vec, tit_vec



    def docs_classification(self, skills_rad=0.6, gamma_occ = 0.74,
                            docs_sample=None, ev_skills=True, ev_occupations=True):
        """
        Description:
            Classify documents
            
        Args:
            skills_rad (float): Radius of accepeted skills.
            gamma_occ (float): Factor of title-description convex sum.
            doc_sample (float): Number of random documnts.
            ev_skills (bool): Classify skills condition
            ev_occupation (bool): Classify occupation condition
            
        Return:
            dictionary:
            {
                'id_doc_pp': Index of pre-processed documnets,
                'id_doc': Original index,
                'country', Country of observation,
                'skills': Identifiers of skills,
                's_cos_dist': cosine distances of skills,
                'occupations': Identifiers of occupations,
                'o_cos_dist': cosine distances of skills,
            }
            
        """

        cos = torch.nn.CosineSimilarity(dim=0, eps=1e-9)   # Cosine distances instance
        
        if docs_sample and docs_sample < len(self.doc_text_filter):
            # Select doc_sample random documents
            index_ev_h = np.random.choice(len(self.doc_text_filter),
                                          size=docs_sample,
                                          replace=False
                                        )
        else:
            index_ev_h = np.array(range(len(self.doc_text_filter)))

        '''
        Get lables and index.
        '''
        s_final_labels = self._l_manager_s.new_labels_docs

        o_final_labels = self._l_manager_o.new_labels_docs

        id_labels_s = self._l_manager_s.id_labels
        id_labels_o = self._l_manager_o.id_labels
        
        
        res = [ ]    # Inital response
        
        # Init classification
        for i, (index, tit, doc, country) in enumerate(tqdm(np.array(self.doc_text_filter)[index_ev_h])):
            
            resultado = { 
                    'id_doc_pp': index_ev_h[i],
                    'id_doc': index,
                    'country': country,
            }
            
            
            # Init skills classification
            if ev_skills:
                try:
                    text_vec, tit_vec = self.__text_embedding(tit, doc, False)
                except:
                    continue

                rad = 1 - skills_rad
                alpha =  0
                text_vec = alpha*tit_vec + (1-alpha)*text_vec

                dists = np.zeros(len(id_labels_s))
                for j in range(len(id_labels_s)):

                    label = s_final_labels[j].split()    # Label
                    label_bow = self._dct_skills.doc2bow(label)    # Label BoW
                    label_vec = self._lsi_skills[label_bow]    # Label embedding
                    label_vec = torch.Tensor(np.array(label_vec)[:, 1])    # Torch tensor

                    dist = cos(text_vec, label_vec)    # Cosine distance
                    dists[j] = dist.item()

                topk = np.argwhere(dists > rad).flatten()    # Take distnaces into the radius
                cos_dists = dists[topk]
                if len(topk) > 2:
                    aux_idx = np.flip(np.argsort(cos_dists)).astype(int)

                    topk = topk[aux_idx]    # Sort identifiers
                    cos_dists =  cos_dists[aux_idx]    # Sort distances
                else:
                    # Top k
                    dists = torch.from_numpy(dists)
                    cos_dists, topk = torch.topk(dists, 5)
                
                '''
                Add results
                '''
                resultado.update({
                    'skills': np.array(id_labels_s)[topk],
                    's_cos_dist': cos_dists
                })

            # Init occupation classification
            if ev_occupations:
                try:
                    text_vec, tit_vec = self.__text_embedding(tit, doc, True)
                except:
                    continue
                
                alpha = gamma_occ
                text_vec = alpha*tit_vec + (1-alpha)*text_vec    # Convex sum

                dists = torch.zeros(len(id_labels_o))
                for j in range(len(id_labels_o)):

                    label = o_final_labels[j].split()    # Label
                    label_bow = self._dct_occ.doc2bow(label)    # Label BoW
                    label_vec = self._lsi_occ[label_bow]  # Label embedding
                    # print(np.array(label_vec)[:, 0])
                    label_vec = torch.Tensor(np.array(label_vec)[:, 1])   # Torch tensor

                    dist = cos(text_vec, label_vec)
                    dists[j] = dist
                    
                cos_dists, topk = torch.topk(dists, 3)    # Top k
                
                '''
                Add results
                '''               
                resultado.update({
                    'occupations': np.array(id_labels_o)[topk],
                    'o_cos_dist': cos_dists
                })
            
            '''
            Append results
            '''
            res.append(resultado)

        return res
    

    def results_as_df(self, results):
        
        _a = results
        ids = []
        tits = []
        ctrys = []
        descs = []
        occs = []
        skills = []
    
        for h in tqdm(range(len(_a))):
            id_texto = _a[h].get('id_doc')
            obs = self.sample_df.loc[
                                        self.sample_df.id == id_texto,
                                        ['titulo', 'descripcion', 'country']
                                    ].values
            
            if len(obs) > 1:
                country_aux = obs[:, -1]
                for c, ctry in enumerate(country_aux):
                    if ctry == _a[h].get('country'):
                        obs = [obs[c]]
                        break
            if len(obs) > 1:
                continue
            
            titulo_vacante, descripcion_vac, country_vac = obs[0]
            tits.append(titulo_vacante)
            descs.append(descripcion_vac)
            ctrys.append(country_vac)
            ids.append(id_texto)

            aux_1 = ''
            for (id_occ, cos_dist) in list(zip(_a[h].get('occupations', ''), _a[h].get('o_cos_dist', ''))):
                aux_1 += f'{id_occ}: {self._o_manager.main_tree.get_node(id_occ).tag} ({cos_dist}) \n'
                
            occs.append(aux_1)
            
            aux_2 = ''
            for (id_skill, cos_dist) in list(zip(_a[h].get('skills', ''), _a[h].get('s_cos_dist', ''))):
                aux_2 += f'{id_skill}: {self._s_manager.main_tree.get_node(id_skill).tag} ({cos_dist})\n'
        
            skills.append(aux_2)
            
        to_pandas = {
            'id': ids,
            'Titulo': tits,
            'Country': ctrys,
            'Descripción': descs,
            'Ocupaciones': occs,
            'Habilidades': skills
        }

        df = pd.DataFrame(data=to_pandas)
        
        return df

        