#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Author: Quantil - Data Mining Group
"""

import torch
import numpy as np
import torch.nn as nn
import torch.optim as optim
from tqdm import tqdm
from torch.utils.data import DataLoader
from .GloVe import GloVeModel
from .tools import CustomDictionary
import pickle
from torch.utils.tensorboard import SummaryWriter


class Trainer:
    def __init__(self, args):
        """ Handler for GloVe model. Instantiates GloVe, then fits corpus to count cooccurance matrix,
            creates DataLoader for batch training, and finally defines optimizer.

        Args:
            args: Parser for command-line options, arguments and sub-commands
        """
        
        # Load data
        self.args = args
        
        self.dict_path = f'./data_default/{self.args.train_mode}/token2id.pth'
        self.files_path = f'./data_default/{self.args.train_mode}/files.pth'
        
        self.writer = SummaryWriter(f'GloVe/ckpt/{self.args.train_mode}/logs/', flush_secs=3)
        
        self.corpus, self.vocab_size, self.dictionary = self.build_dict()
        self.model = GloVeModel(args.embedding_len, 
                                args.context_size, 
                                self.vocab_size,
                                self.dict_path).to(args.device)
        
        # fit corpus to count cooccurance matrix
        self.model.fit(self.corpus)
        self.dataloader = DataLoader(self.model._glove_dataset, 
                                     batch_size=args.batch_size,
                                     shuffle=True)
        self.optim = optim.Adam(self.model.parameters(), lr=args.lr)
        print('\nTraining on device: {}'.format(self.args.device))
        
        if self.args.fine_tuning == True:
            print('Loading latest checkpoint found to fine tune the model')
            ckpt_path = f'GloVe/ckpt/{self.args.train_mode}/saved_models/epoch_69_ckpt.pth'
            self.model.load_model(ckpt_path)
            print('Finished Loading latest checkpoint\n')
        
        
    def build_dict(self):
        """ Get corpus and vocab_size from preprocessed data

        Returns:
            corpus (list): list of idx words
            vocab_size (int): vocabulary size
        """
        print(f'Beginning to create dictionary')
        dictionary = CustomDictionary(self.dict_path)
        files = torch.load(self.files_path)['files']
        corpus = dictionary.corpus(files)
        vocab_size = dictionary.vocab_size
        with open(f'GloVe/ckpt/{self.args.train_mode}/dictionary.pickle', mode='wb') as fp:
            pickle.dump(dictionary, fp)
        print(f'Finished creating dictionary\n')
        
        return corpus, vocab_size, dictionary
       
    def train(self):
        """ Trains GloVe model for specified number of epochs.
        """
        
        # saving cooccurance_matrix
        cooccurance_matrix = self.model.get_coocurrance_matrix()
        with open(f'GloVe/ckpt/{self.args.train_mode}/coocc_matrix.pickle', mode='wb') as fp:
            pickle.dump(cooccurance_matrix, fp)
        
        if self.model._glove_dataset is None:
            raise NotFitToCorpusError(
                "Please fit model with corpus before training")
            
        for epoch in tqdm(range(self.args.epochs)):
            print(f'Beginning epoch: {epoch + 1}/{self.args.epochs}')
            running_loss = 0.0
            global_step = epoch * len(self.dataloader)
            num_examples = 0
            
            epoch_loss = []
            for idx, batch in enumerate(tqdm(self.dataloader)):
                # Unpack data
                [i_s, j_s, counts] = [d.to(self.args.device) for d in batch]
                
                # Remove accumulated gradients
                self.optim.zero_grad()
                # Calc loss
                loss = self.model.loss(i_s, j_s, counts)
                                
                # Backprop and update
                loss.backward()
                self.optim.step()
                
                # Keep track of loss          
                running_loss += loss.item()
                global_step += 1
                
            epoch_loss.append(running_loss / self.args.batch_size)         
            self.log_step(epoch, global_step, running_loss / self.args.batch_size)
            
            if epoch % self.args.save_every == 0:
                self.save_epoch(epoch, running_loss / self.args.batch_size)
        
        print('Saving training loss for further processing...')
        torch.save({
            'train_loss': epoch_loss
        }, f'GloVe/ckpt/{self.args.train_mode}/train_loss.pth')
        
        self.save_epoch(epoch, running_loss / self.args.batch_size)
        print('Finished training GloVe!')
        
    def save_epoch(self, epoch, loss):
        # Save checkpoint
        print(f'Beginning to save checkpoint')
        torch.save({
            'epoch': epoch + 1,
            'model_state_dict': self.model.state_dict(),
            'optimizer_state_dict': self.optim.state_dict(),
            'loss': loss,
        }, f'GloVe/ckpt/{self.args.train_mode}/saved_models/epoch_{epoch}_ckpt.pth')
        print(f'Finished saving checkpoint')
        
    def log_step(self, epoch, global_step, loss):
        print(f'#############################################')
        print(f'EPOCH: {epoch} | STEP: {global_step} | LOSS {loss}')
        print(f'#############################################')
        
        self.writer.add_scalar('train_loss', loss, global_step)
        
        # Visualize document embeddings
        self.writer.add_embedding(
            self.model.embeddings(),
            global_step=epoch,
            tag=f'we_epoch_{epoch}',
        )
                        
class NotFitToCorpusError(Exception):
    pass