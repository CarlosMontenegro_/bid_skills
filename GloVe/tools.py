#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Author: Quantil - Data Mining Group
"""

import torch

class CustomDictionary:
    
    def __init__(self, dict_path):
        """ Tool to build word2idx and doc2idx
    
        Args:
            dict_path {str}: path to the clean documents
        """

        self.word2idx = torch.load(dict_path)['token2id']
        self.vocab_size = len(self.word2idx)

    def corpus(self, doc: list) -> list:
        """ Convert text of documents to idx of documents
        
        Args:
            doc (list): text of documents
        Returns:
            corpus: idx of documents
        """

        word2idx = self.word2idx
        corpus = [[word2idx[word] for word in line if word in word2idx]
                  for line in doc]
        return corpus

    
def _get_GloVe_vecs(path):
    """Gets the word embeddings from pre-trained GloVe model.
        
    Args:
        path (str): path to trained model.
    Returns:
        glove_vecs (torch.tensor): word embeddings of size (no. of tokens) x (embedding_dim)
    """
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    state_dict = torch.load(path, map_location=device)
    
    focal_embeds = state_dict['model_state_dict']['_focal_embeddings.weight']
    context_embeds = state_dict['model_state_dict']['_context_embeddings.weight']
    glove_vecs = 0.5*(focal_embeds + context_embeds)
    
    return glove_vecs

def get_tokens(dict_path='./data_default/lemma/token2id.pth'):
    """Get a list of the tokens found in the dictionary
        
    Args:
        dict_path (str): path to dictionary (tokens to id).
    Returns:
        (list): tokens found in the dictionary
    """
    return torch.load(dict_path)['token2id'].keys()

def get_similarity(word, 
                   kNN, 
                   model_path='GloVe/ckpt/lemma/saved_models/epoch_69_ckpt.pth', 
                   dict_path='./data_default/lemma/token2id.pth'
                  ):
    """Finds k-Nearest Neighbors to the given word.
        
    Args:
        word (str): word to find k-NN (must be included in one the the token2id keys)
        kNN  (int): k-Nearest Neighbors
        model_path (str): path to trained model
        dict_path (str): path to dictionary (tokens to id).
    Returns:
        nearest_neighbors (list): nearest words to word using cosine similarity
    """
    try:
        W = _get_GloVe_vecs(model_path)
        token2id = torch.load(dict_path)['token2id']

        x = W[token2id[word]]
        cos = torch.matmul(W, x) /(torch.sum(W * W, dim=-1) \
                                   * torch.sum(x * x) + 1e-9).sqrt()
        _, topk = torch.topk(cos, kNN+1)

        id2token = {idx: token for idx, token in enumerate(token2id.keys())}
        nearest_neighbors = topk[1:].tolist()
        nearest_neighbors = [id2token[comp] for comp in nearest_neighbors]

        return nearest_neighbors
    
    except:
        print('Something went wrong! Make sure the word is in the token2id keys.')
        print('Or, check the paths to the GloVe checkpoint and dictionary once again.')

def analogies(word1, 
              word2, 
              word3, 
              kNN=1,
              model_path='GloVe/ckpt/lemma/saved_models/epoch_69_ckpt.pth', 
              dict_path='./data_default/lemma/token2id.pth'
             ):
    """Analogies: king (word1) - man(word2) + woman(word3) = ?
        
    Args:
        word1 (str): must be included in one the the token2id keys
        word2 (str): must be included in one the the token2id keys
        word3 (str): must be included in one the the token2id keys
        kNN  (int): k-Nearest Neighbors
    Returns:
        nearest_neighbors (list): analogy
    """
    try:
        W = _get_GloVe_vecs(model_path)
        token2id = torch.load(dict_path)['token2id']

        w1 = W[token2id[word1]]
        w2 = W[token2id[word2]]
        w3 = W[token2id[word3]]

        x = w1 - w2 + w3

        cos = torch.matmul(W, x) /(torch.sum(W * W, dim=-1) \
                                   * torch.sum(x * x) + 1e-9).sqrt()
        _, topk = torch.topk(cos, kNN + 1)

        id2token = {idx: token for idx, token in enumerate(token2id.keys())}
        nearest_neighbors = topk[1:].tolist()
        nearest_neighbors = [id2token[comp] for comp in nearest_neighbors]

        return nearest_neighbors
    
    except:
        print('Something went wrong! Make sure the words are in the token2id keys.')
        print('Or, check the paths to the GloVe checkpoint and dictionary once again.')