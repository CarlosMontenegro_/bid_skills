#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Author: Quantil - Data Mining Group
"""

import sys
sys.path.append("..")

import argparse
from .trainer import Trainer
import torch as t
import pandas as pd
from processingText import ProcessingText
from gensim.corpora import Dictionary


def get_args():
    parser = argparse.ArgumentParser(description="PyTorch GloVe Training")
    
    """
    Data handling
    """
    parser.add_argument('--train_mode', type=str, default='lemma', help='if data was lemmatized choose: lemma\
                        else choose: no_lemma')
    parser.add_argument('--path_data', type=str, default='', help='path to csv database if you want to train with new job posts.')

    """
    Model Parameters
    """
    parser.add_argument('--embedding_len', type=int, default=300, help='Length of\
                        embeddings in model (default: 300)')
    parser.add_argument('--context_size', type=int, default=5, help='(Symmetric) Window to\
                        build the co-ocurrence matrix (default: 5)')
    parser.add_argument('--no_below', type=int, default=100, help='Filter out tokens that appear in\
                        less than no_below documents (absolute number).')
    parser.add_argument('--no_above', type=float, default=0.85, help='Filter out tokens that appear in\
                        more than no_above documents (fraction of total corpus size, not absolute number).')
    
    """
    Training Hyperparameters
    """
    parser.add_argument('--epochs', type=int, default=70, metavar='N',
                        help='Number of epochs to train for - iterations over the dataset (default: 70)')
    parser.add_argument('--batch_size', type=int, default=1024,
                        metavar='N', help='Number of examples in a training batch (default: 1024)')
    parser.add_argument('--lr', type=float, default=1e-3, metavar='LR',
                        help='Learning rate (default: 1e-3)')
    parser.add_argument('--seed', type=int, default=42, metavar='S',
                        help='Random seed (default: 42)')
    
    """
    Checkpoint Options
    """
    parser.add_argument('--save_every', type=int, default=10, help='How often (epochs) to save models in\
                        training. (default: 10)')

    """
    Training Settings
    """
    parser.add_argument('--device', type=str, default=t.device("cuda:0" if t.cuda.is_available() else "cpu"),
                        help='Device to train on (default: cuda:0 if cuda is available otherwise cpu)')
    parser.add_argument('--fine_tuning', type=bool, default=True, help='Specify whether you want to\
                        fine tune the model: start training from a previous checkpoint (default: True)')
    
    return parser.parse_args()

if __name__ == '__main__':
    args = get_args()
    
    if args.path_data != '':
        # Load csv files
        sample_df = pd.read_csv(args.path_data)
        
        # We are interested in the job descriptions only, so take that column
        files = sample_df['descripcion']
        files.dropna(inplace=True)
        files = list(files.to_numpy())
        
        # Clean text
        cleaner = ProcessingText()
        data_lemmatized = cleaner.processing_corpus(files, lemma=False, tqdm_track=True)
        
        # Create dictionary
        id2word = Dictionary(data_lemmatized)
        
        if args.no_below == 0 or args.no_above == 0:
            raise ValueError(
                "You should specify 'no_below' and 'no_above' to build the dictionary")
            
        id2word.filter_extremes(no_below=args.no_below, 
                                no_above=args.no_above, 
                                keep_n=None)
        token2id = id2word.token2id
        
        t.save({
                'files': data_lemmatized
        }, f'./data_default/{args.train_mode}/files.pth')
        
        t.save({
                'token2id': token2id
        }, f'./data_default/{args.train_mode}/token2id.pth')
        
    
    trainer = Trainer(args)
    
    # Begin Training!
    trainer.train()