#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Author: Quantil - Data Mining Group
"""

from collections import Counter, defaultdict
import torch
import torch.nn as nn
import torch.nn.init as init
import torch.optim as optim
from torch.utils.data import Dataset
from tqdm import tqdm


class GloVeModel(nn.Module):
    """Implement GloVe model with Pytorch
    """

    def __init__(self, embedding_size, context_size, vocab_size, dict_path,
                 min_occurrance=1, x_max=100, alpha=0.75):
        """ Init GloVe model.
    
        Args:
            embedding_size (int): Size of embeddings in model
            context_size (int): (Symmetric) Window to build the co-ocurrence matrix
            vocab_size (int): Vocabulary size
            dict_path (str): Path to dictionary (after preprocessing)
            min_occurrance (int): Minimum occurrance of word to build the co-ocurrence matrix
            x_max (int): Normalization for the co-occurrence count in the weighting factor for weighted least-squares loss
            alpha (float): Exponentiation parameter to sample least frequent words in loss calculation
            
        Raises:
            ValueError: when context_size is not a integer or tuple of integers
        """
        super(GloVeModel, self).__init__()

        self.embedding_size = embedding_size
        if isinstance(context_size, tuple):
            self.left_context, self.right_context = context_size
        if isinstance(context_size, int):
            self.left_context = self.right_context = context_size
        else:
            raise ValueError(
                "'context_size' should be an int or a tuple of two ints")
        self.vocab_size = vocab_size
        self.alpha = alpha
        self.min_occurrance = min_occurrance
        self.x_max = x_max

        self._focal_embeddings = nn.Embedding(
            vocab_size, embedding_size).type(torch.float64)
        self._context_embeddings = nn.Embedding(
            vocab_size, embedding_size).type(torch.float64)
        self._focal_biases = nn.Embedding(vocab_size, 1).type(torch.float64)
        self._context_biases = nn.Embedding(vocab_size, 1).type(torch.float64)
        self._glove_dataset = None
        
        self.token2id = torch.load(dict_path)['token2id']

        for params in self.parameters():
            init.uniform_(params, a=-1, b=1)

    def fit(self, corpus):
        """Get dictionary word list and co-occruence matrix from corpus
        
        Args:
            corpus (list): contains word id list
        Raises:
            ValueError: when count zero cocurrences will raise the problems
        """
        print(f'Beginning of fitting: we get dictionary word list and co-occruence matrix from corpus.')
        left_size, right_size = self.left_context, self.right_context
        vocab_size, min_occurrance = self.vocab_size, self.min_occurrance

        # get co-occurence count matrix
        word_counts = Counter()
        cooccurence_counts = defaultdict(float)
        for region in tqdm(corpus):
            word_counts.update(region)
            for left_context, word, right_context in _context_windows(region, left_size, right_size):
                for i, context_word in enumerate(left_context[::-1]):
                    # add (1 / distance from focal word) for this pair
                    cooccurence_counts[(word, context_word)] += 1 / (i + 1)
                for i, context_word in enumerate(right_context):
                    cooccurence_counts[(word, context_word)] += 1 / (i + 1)
        if len(cooccurence_counts) == 0:
            raise ValueError(
                "No coccurrences in corpus, Did you try to reuse a generator?")

        # get words bag information
        print('Get tokens')
        tokens = [word for word, count in
                  tqdm(word_counts.most_common(vocab_size)) if count >= min_occurrance]
        print('Computing our co-occruence matrix')
        coocurrence_matrix = [(words[0], words[1], count)
                              for words, count in tqdm(cooccurence_counts.items())
                              if words[0] in tokens and words[1] in tokens]
        print(f'Done fitting.\n')
        self._glove_dataset = GloVeDataSet(coocurrence_matrix)
    
    def get_coocurrance_matrix(self):
        """ Returns co-occurance matrix for saving
        
        Returns:
            list: list item (word_idx1, word_idx2, cooccurances)
        """
        return self._glove_dataset._coocurrence_matrix

    def embeddings(self):
        """ Returns word embeddings for the words in the vocabulary.
        
        Returns:
            torch tensor: tensor containing word embeddings. As many rows as words and columns as embedding_size.
        """
        
        return 0.5 * (self._focal_embeddings.weight.data.cpu() + 
                         self._context_embeddings.weight.data.cpu() )

    def loss(self, focal_input, context_input, coocurrence_count):
        x_max, alpha = self.x_max, self.alpha

        focal_embed = self._focal_embeddings(focal_input)
        context_embed = self._context_embeddings(context_input)
        focal_bias = self._focal_biases(focal_input)
        context_bias = self._context_biases(context_input)

        # count weight factor
        weight_factor = torch.pow(coocurrence_count / x_max, alpha)
        weight_factor[weight_factor > 1] = 1

        embedding_products = torch.sum(focal_embed * context_embed, dim=1)
        log_cooccurrences = torch.log(coocurrence_count)

        distance_expr = (embedding_products + focal_bias +
                         context_bias - log_cooccurrences) ** 2

        single_losses = weight_factor * distance_expr
        mean_loss = torch.mean(single_losses)
        return mean_loss
    
    def load_model(self, path):
        """Loads trained model stored in path
        
        Args:
            path (str): path to the saved .pth model
        """
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        state_dict = torch.load(path, map_location=device)
        self.load_state_dict(state_dict['model_state_dict'])

class GloVeDataSet(Dataset):
    def __init__(self, coocurrence_matrix):
        self._coocurrence_matrix = coocurrence_matrix

    def __getitem__(self, index):
        return self._coocurrence_matrix[index]

    def __len__(self):
        return len(self._coocurrence_matrix)
    

def _context_windows(region, left_size, right_size):
    """Generate left_context, word, right_context tuples for each region
    
    Args:
        region (str): a sentence
        left_size (int): left windows size
        right_size (int): right windows size
    """

    for i, word in enumerate(region):
        start_index = i - left_size
        end_index = i + right_size
        left_context = _window(region, start_index, i - 1)
        right_context = _window(region, i + 1, end_index)
        yield (left_context, word, right_context)
        
def _window(region, start_index, end_index):
    """Returns the list of words starting from `start_index`, going to `end_index`
    taken from region. If `start_index` is a negative number, or if `end_index`
    is greater than the index of the last word in region, this function will pad
    its return value with `NULL_WORD`.
    
    Args:
        region (str): the sentence for extracting the token base on the context
        start_index (int): index for start step of window
        end_index (int): index for the end step of window
    """
    last_index = len(region) + 1
    selected_tokens = region[max(start_index, 0):
                             min(end_index, last_index) + 1]
    return selected_tokens
