**bid_skills: Caracterización de la demanda en el mercado laboral de habilidades básicas, avanzadas, sociales, cognitivas y digitales en cinco países de Latinoamérica**

---

## Descripción y contexto

El objetivo del proyecto es construir una herramienta que ayude al estudio la demanda en el mercado laboral de habilidades básicas y avanzadas, sociales, cognitivas y digitales en cinco países de Latinoamérica. Como insumo para este trabajo se usó 2.8 millones de descripciones laborales en estos países, recolectadas para el 2017 y 2018. Se utilizaron metodologías de aprendizaje de máquinas, en particular de procesamiento de lenguaje natural, sobre las descripciones laborales para extraer y clasificar las habilidades de cada vacante de acuerdo con la taxonomía de ESCO de "Ocupaciones" y "Habilidades/Competencias".

---

## Guía de usuario

En el archivo adjunto `Manual de usuario.pdf` encuentra la documentación del paquete así como la guía de uso.

---

## Guía de instalación

### Dependencias
Adjunto también encuentra un archivo `requirements.txt` con los paquetes usados en la máquina donde se desarrolló el código. Para instalarlos, ejecute los siguientes comando desde un `command-line`

    git clone https://CarlosMontenegro_@bitbucket.org/CarlosMontenegro_/bid_skills.git
    cd bid_skills
    pip install -r requirements.txt
    
## Autor/es
---
Quantil S.A.S es una compañía de matemáticas aplicadas a la industria. Fue fundada en el año 2008 y se apoya en dos pilares fundamentales: capital humano y una aproximación científica al análisis y solución de problemas. Para ofrecer servicios de la más alta calidad técnica nos rodeamos del más alto capital humano. Nuestro equipo cuenta con años de preparación académica y profesional en diferentes disciplinas científicas. Quantil, por medio de sus socios e investigadores, está en capacidad de mezclar teorías matemáticas sofisticadas con las necesidades prácticas de la industria, para obtener productos de calidad, aplicables, y útiles.