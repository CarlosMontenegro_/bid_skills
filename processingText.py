import pandas as pd 
import numpy as np

## Normlaize text method
from gensim.parsing.preprocessing import split_alphanum, strip_multiple_whitespaces, \
                                            strip_non_alphanum, strip_numeric, strip_punctuation, \
                                                strip_tags
import re 

## Tokenize text methods
import nltk
from nltk.tokenize import word_tokenize

## Filter part
from nltk.corpus import stopwords

## Lemmatize and setmmize methods
from nltk.stem import WordNetLemmatizer, SnowballStemmer

from tqdm import tqdm

class ProcessingText():

    def __init__(self):
        ## atributo de la clase
        self.lemmatizer = WordNetLemmatizer()
        self.stemmer = SnowballStemmer('spanish')


    '''
    ----------------- FIRST PART -> PREPROCESSING ------------------------
    '''

    def __clean_text(self, sent):
        replacements = (
            ("á", "a"),
            ("é", "e"),
            ("í", "i"),
            ("ó", "o"),
            ("ú", "u"),
            ("Á", "a"),
            ("É", "e"),
            ("Í", "i"),
            ("Ó", "o"),
            ("Ú", "u"),
            ("Ñ", "nh"),
            ("ñ", "nh")    
            )

        for a, b in replacements:
            sent = sent.replace(a, b)

        return sent
    def __anti_cons(self, s):
        x  = re.findall(r'br+[b-df-hj-np-tv-xyz]', s)
        for i in range(len(x)):
            letra = x[i][-1]
            s = re.sub(r'br+[b-df-hj-np-tv-xyz]', ' ' + letra, s, count=1)

        y = re.findall(r'[b-df-hj-lnp-tv-xyz]+br', s)
        for j in range(len(y)):
            letra = y[j][0]
            s = re.sub(r'[b-df-hj-lnp-tv-xyz]+br', letra + ' ', s, count=1)

        return s

    def __clean_sent(self, sent):
        text = []
        for word in sent.split():
            w = word if not word[-2:] == 'br' else word[:-2]
            text.append(w)

        return ' '.join(text)

    # Método gneral para normalizar un texto a su estado trabajable
    def normalize(self, sent, 
                html_tags=False, 
                spanish_char=True, 
                multi_encode=False):

        # Remove Emails
        sent = re.sub(r'\S*@\S*\s?', '', sent)
        # Remove tildes y Ñ

        if spanish_char:
            sent = self.__clean_text(sent)

        if multi_encode:
            sent = sent.encode('ascii', 'ignore').decode('utf-8')

        # ...=... all
        sent = re.sub(r'\S*=\S*\s?', '', sent)

        # Add spaces between digits & letters in s using RE_AL_NUM.
        sent = split_alphanum(sent)
        # Remove repeating whitespace characters (spaces, tabs, line breaks) 
        # from s and turns tabs & line breaks into spaces using RE_WHITESPACE.
        sent = strip_multiple_whitespaces(sent)
        # Remove non-alphabetic characters from s using RE_NONALPHA.
        sent = strip_non_alphanum(sent)
        # Remove digits from s using RE_NUMERIC.
        sent = strip_numeric(sent)
        # Replace punctuation characters with spaces in s using RE_PUNCT.
        sent = strip_punctuation(sent)
        # Remove tags from s using RE_TAGS.
        sent = strip_tags(sent)

        if html_tags:
            sent = self.__anti_cons(sent)
            sent = self.__clean_sent(sent)

        return sent

    '''
    ----------------- SECOND PART -> TOKENIZE ----------------------------
    '''

    def doc2token(self, doc):
        doc_tokens = word_tokenize(doc)

        return doc_tokens

    '''
    ----------------- THIRD PART -> FILTER WORDS -------------------------
    '''

    ## Recibe el docuemtno tokenizado
    def remove_tw(self, doc_token, aditional_stop_word=[],
                    language='spanish', min_len_word=3):

        ## If doc is not tokenized raise exception.
        if not type(doc_token) == list:
            raise Exception('Only accept docs after tokenize process')

        stop_words = stopwords.words(language) # Stop words por defecto

        if aditional_stop_word:
            stop_words = stop_words.extend(aditional_stop_word)

        ## Remove condition
        condition = lambda w: True if (w in stop_words) or len(w) <= min_len_word else False

        ## New doc
        new_doc_token = [w for w in doc_token if not condition(w)]

        return new_doc_token

    '''
    ----------------- FOURTH PART -> LEMMATIZE AND STEMMIZE  -------------
    '''

    ## Lematizer
    def lemmatizer_word(self, word):
        ## If doc is not tokenized raise exception.
        if not type(word) == str:
            raise Exception('Only accept String type')

        lemm_word = self.lemmatizer.lemmatize(word, 'v')

        return lemm_word

    def lemmatizer_stemmer_word(self, word):
        res = None
        dict_lemm = { 'ingenieria': 'ingenier'}
        b_list = list(dict_lemm.keys())
        s_list = list(dict_lemm.values())
        if word in b_list:
            res = s_list[b_list.index(word)]
        else:
            res = self.stemmer.stem(self.lemmatizer_word(word))

        return res

    '''
    ----------------- ALL PROCESS  ---------------------------------------
    '''

    ## Mejorar y hacer los condicionales segun el procedimiento que se quiere
    def processing_corpus(self, corpus,
        min_word_voc=3,
        tqdm_track=False,
        lemma=True):
        """
            Description:
                Perform whole preprocessing steps for a given corpus.
            Args:
                corpus (array of str): Set of documents
                min_word_voc (int): Min allowed lenght of each word
                tqdm_track (bool): Condition of tracking process
                lemma (bool): Condition of lemmaizer
            Returns:
                Processed Corpus: Array of array of preprocessed words.
        """
        clean_corpus = []

        corpus = tqdm(corpus, desc='Procesando Corpus') if tqdm_track else corpus

        for doc in corpus:
            doc = self.normalize(doc, html_tags=True, multi_encode=True)

            doc_token = self.doc2token(doc)
            clean_doc_token = self.remove_tw(doc_token, min_len_word=min_word_voc)

            # clean_doc = list(map(lambda w: self.lemmatizer_stemmer_word(w), clean_doc_token))
            if lemma:
                clean_doc = [self.lemmatizer_stemmer_word(w) for w in clean_doc_token]
            else:
                clean_doc = clean_doc_token

            clean_corpus.append(clean_doc)

        return clean_corpus